export const fetchAllPokemon = () => {
  return $.ajax("api/pokemon");
};

export const fetchPokemon = (id) => {
  return $.ajax(`api/pokemon/${id}`);
};
