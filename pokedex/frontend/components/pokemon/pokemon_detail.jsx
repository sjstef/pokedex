import React from 'react';
import { Link } from 'react-router-dom';
import { Route } from 'react-router-dom';
import ItemDetailContainer from './item_detail_container';

export default class PokemonDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = { id: parseInt(this.props.match.params.pokemonId)};
  }

  componentDidMount() {
    this.props.requestPokemon(this.state.id);
  }

  componentWillReceiveProps(newProps) {
    const oldId = this.state.id;
    this.state.id = parseInt(newProps.match.params.pokemonId);
    if (oldId != this.state.id) {
      this.props.requestPokemon(this.state.id);
    }
  }

  render() {
    const poke = this.props.pokemon[this.state.id];
    if (!poke) return <div></div>;
    // if (!poke.moves) return <div></div>;
    poke.moves = poke.moves || [];
    poke.itemIDs = poke.itemIDs || [];
    const items = poke.itemIDs.map((itemId) => {
      return (
        <li key={itemId}>
          <Link to={`${this.props.match.url}/items/${itemId}`}>
            <img src={this.props.items[itemId].image_url} width="100" height="100"/>
          </Link>
        </li>
      );
    });
    return (
      <div class="pokemon-detail">
        <figure>
          <img src={poke.image_url} width="600" height="400"></img>
        </figure>
        <ul>
          <li><h2>{poke.name}</h2></li>
          <li><h3>Type: {poke.type}</h3></li>
          <li><h3>Attack: {poke.attack}</h3></li>
          <li><h3>Defense: {poke.defense}</h3></li>
          <li><h3>Moves: {poke.moves.join(", ")}</h3></li>
        </ul>
        <section>
          <h2>Items</h2>
          <ul class='item-list'>
            {items}
          </ul>
          <Route path={`/pokemon/${poke.id}/items/:itemId`}
            component={ ItemDetailContainer } />
        </section>
      </div>
    );
  }
}
