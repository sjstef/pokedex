import React from 'react';
import { Link } from 'react-router-dom';

export default class PokemonIndexItem extends React.Component {
  render () {
    const poke = this.props.pokemon;
    return (
      <li key={poke.id}>
        <Link to={`/pokemon/${poke.id}`}>
          <img src={poke.image_url}/> {poke.name}
        </Link>
      </li>
    );
  }
}
