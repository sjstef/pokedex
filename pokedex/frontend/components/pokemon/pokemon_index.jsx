import React from 'react';
import PokemonIndexItem from './pokemon_index_item';
import { HashRouter, Route } from 'react-router-dom';
import PokemonDetailContainer from './pokemon_detail_container';

export default class PokemonIndex extends React.Component {
  componentDidMount () {
    this.props.requestAllPokemon();
  }

  render(){
    const pokemonLis = this.props.pokemon.map(poke => <PokemonIndexItem key={poke.id} pokemon={poke} />);

    return (
      <div class="pokedex">
        <ul>
          {pokemonLis}
        </ul>
        <HashRouter><Route path="/pokemon/:pokemonId" component={ PokemonDetailContainer } /></HashRouter>
      </div>
    );
  }
}
