import { RECEIVE_ALL_POKEMON, RECEIVE_POKEMON } from '../actions/pokemon_actions';

export default function pokemonReducer(state = {}, action) {
  switch(action.type) {
    case RECEIVE_ALL_POKEMON:
      return Object.assign({}, action.pokemon, state);

    case RECEIVE_POKEMON:
      const newState = Object.assign({}, state);
      let id = action.pokemon.id;

      newState[id] = action.pokemon;
      newState[id].itemIDs = action.items.map( (item) => {
        return item.id;
      });
      return newState;
    default:
      return state;
  }
}
