import { RECEIVE_POKEMON } from '../actions/pokemon_actions';

const itemReducer = (state = {}, action) => {
  switch (action.type) {
    case RECEIVE_POKEMON:
      const newState = Object.assign({}, state);
      action.items.forEach( (item) => {
        newState[item.id] = item;
      });
      return newState;
    default:
      return state;

  }
};

export default itemReducer;
