import { RECEIVE_POKEMON } from '../actions/pokemon_actions';

const uiReducer = (state={}, action) => {
  switch(action.type) {
    case RECEIVE_POKEMON:
      return { pokeDisplay: action.pokemon.id };
    default:
      return state;
  }
};

export default uiReducer;
