json.array! @parties do |party|
  json.partial! "party", party: party, show_gift: false
end
